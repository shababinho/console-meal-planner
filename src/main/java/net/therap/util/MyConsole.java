package net.therap.util;

import net.therap.dao.MealDao;
import net.therap.domain.Meal;

import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.stream.IntStream;

/**
 * @author shabab
 * @since 3/12/18
 */
public class MyConsole {
    private final String weekDays[];
    private MealDao mealDao;

    public MyConsole() {
        weekDays = new String[]{"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday"};
        mealDao = new MealDao();
    }

    public void startPrompting() {
        System.out.println("----\tWelcome to Meal Planner\t----");
        boolean looper = true;
        Scanner scanner = new Scanner(System.in);
        while (looper) {
            System.out.println("Enter any command of the following");
            System.out.printf("1.\tShow meals\n2.\tAdd Item to a day.\n3.\tEdit Meal\n" +
                    "4.\tDelete Meal\n5.Exit\t\n");
            int command = scanner.nextInt();
            switch (command) {
                case 1:
                    showAllMeals();
                    break;
                case 2:
                    addItem();
                    break;
                case 3:
                    editMeal();
                    break;
                case 4:
                    deleteMeal();
                    break;
                default:
                    looper = false;
                    break;
            }
        }
    }

    private void deleteMeal() {
        showAllMeals();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Which meal do you want to delete?");
        int id = scanner.nextInt();
        if (mealDao.delete(id)) {
            System.out.println("Successfully deleted item");
        } else {
            System.err.println("Could not delete item");
        }
    }

    private void editMeal() {
        showAllMeals();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Which meal do you want to update?");
        int id = scanner.nextInt();
        System.out.println("Enter new description");
        scanner.nextLine();
        String mealName = scanner.nextLine();
        HashMap<String, String> keyMap = new HashMap<>();
        keyMap.put("description", mealName);
        if (mealDao.update(id, keyMap)) {
            System.out.println("Successfully edited meal");
        } else {
            System.err.println("Meal could not be updated");
        }
    }

    private void addItem() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Which day you want to add the item to? Enter the number");
        IntStream.range(1, weekDays.length + 1)
                .mapToObj(index -> String.format("%d. %s", index, weekDays[index - 1]))
                .forEach(System.out::println);
        int day = scanner.nextInt();

        System.out.println("Do you want it for breakfast or lunch? Enter Number");
        System.out.printf("1.\tBreakfast\n2.\tLunch\n");
        int mealType = scanner.nextInt();

        scanner.nextLine();
        System.out.println("Describe the item");
        String description = scanner.nextLine();

        Meal meal = new Meal(description, day, mealType);
        if (mealDao.save(meal)) {
            System.out.println("Meal saved");
        } else {
            System.err.println("Could not save meal to database");
        }

    }

    private void showAllMeals() {
        List<Meal> mealList = mealDao.getAll();
        System.out.println("ID.\tDay\tMeal Type\tDescription");
        mealList.stream().forEach(System.out::println);
    }

}
