package net.therap.util;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shabab
 * @since 3/12/18
 */
public class TemplateEngine {
    private Connection connection;

    public TemplateEngine() {
        connection = createConnection();
    }

    public <T> List<T> query(String sql, MyRowMapper<T> rowMapper) {
        List<T> list = new ArrayList<>();
        createConnection();

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            int index = 1;
            if (resultSet.first()) {
                do {
                    list.add(rowMapper.mapRow(resultSet, index++));
                } while (resultSet.next());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return list;
    }

    public <T> T queryForObject(String sql, MyRowMapper<T> rowMapper) {
        createConnection();

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            return rowMapper.mapRow(resultSet, 0);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return null;
    }

    private Connection createConnection() {
        try {
            if (connection == null || connection.isClosed()) {
                try {
                    Class.forName(Constant.DRIVER_NAME);
                    connection = DriverManager
                            .getConnection(Constant.DB_URL, Constant.USER_NAME, Constant.PASSWORD);
                } catch (SQLException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    private void closeConnection() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
