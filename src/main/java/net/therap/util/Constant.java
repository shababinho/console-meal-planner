package net.therap.util;

/**
 * @author shabab
 * @since 3/11/18
 */
public class Constant {
    public static final String DB_NAME = "MealPlanner";
    public static final String USER_NAME = "root";
    public static final String PASSWORD = "root";
    public static final String PORT_NUMBER = "3306";
    public static final String DRIVER_NAME = "com.mysql.cj.jdbc.Driver";
    public static final String URL = "localhost";
    public static final boolean VERIFY_CERT = false;
    public static final boolean USE_SSL = true;
    public static final String TIMEZONE = "Asia/Dhaka";
    public static final boolean TIMEZONE_SHIFT = true;
    public static final boolean LEGACY_TIMEZONE_CODE = false;

    public static final String DB_URL = "jdbc:mysql://" + URL + ":" + PORT_NUMBER + "/"
            + DB_NAME + "?verifyServerCertificate=" + VERIFY_CERT
            + "&useSSL=" + USE_SSL
            + "&useJDBCCompliantTimezoneShift=" + TIMEZONE_SHIFT
            + "&useLegacyDatetimeCode=" + LEGACY_TIMEZONE_CODE
            + "&serverTimezone=" + TIMEZONE;
}
