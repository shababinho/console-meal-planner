package net.therap.util;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author shabab
 * @since 3/12/18
 */
@FunctionalInterface
public interface MyRowMapper<T> {
    public T mapRow(ResultSet resultSet, int index) throws SQLException;
}
