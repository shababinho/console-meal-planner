package net.therap.domain;

/**
 * @author shabab
 * @since 3/11/18
 */
public class Meal {
    private String description;
    private int mealId;
    private int dayId;
    private int mealType;
    private String day;
    private String mealTime;

    public Meal(int mealId, String description, String day, String mealTime) {
        this.mealId = mealId;
        this.description = description;
        this.day = day;
        this.mealTime = mealTime;
    }

    public Meal(String description, int dayId, int mealType) {
        this.description = description;
        this.dayId = dayId;
        this.mealType = mealType;
    }

    public int getMealId() {
        return mealId;
    }

    public void setMealId(int mealId) {
        this.mealId = mealId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDayId() {
        return dayId;
    }

    public void setDayId(int dayId) {
        this.dayId = dayId;
    }

    public int getMealType() {
        return mealType;
    }

    public void setMealType(int mealType) {
        this.mealType = mealType;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMealTime() {
        return mealTime;
    }

    public void setMealTime(String mealTime) {
        this.mealTime = mealTime;
    }

    @Override
    public String toString() {
        return String.format("%d.\t%s\t%s\t%s\t", getMealId(), getDay(),
                getMealTime(), getDescription());
    }
}
