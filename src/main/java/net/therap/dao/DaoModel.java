package net.therap.dao;

import java.util.HashMap;
import java.util.List;

/**
 * @author shabab
 * @since 3/11/18
 */
public interface DaoModel<T> {

    public boolean save(T object);

    public boolean delete(int id);

    public boolean update(int id, HashMap<String, String> updateMap);

    public List<T> getAll();

    public T find(int id);
}
