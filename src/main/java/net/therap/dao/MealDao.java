package net.therap.dao;

import net.therap.domain.Meal;
import net.therap.util.Constant;
import net.therap.util.TemplateEngine;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

/**
 * @author shabab
 * @since 3/12/18
 */
public class MealDao implements DaoModel<Meal> {
    private TemplateEngine templateEngine;
    private Connection connection;

    public MealDao() {
        this.templateEngine = new TemplateEngine();
        this.connection = createConnection();
    }

    @Override
    public boolean save(Meal meal) {
        String mealsTableQuery = "INSERT INTO meals(dayID, mealType) VALUES(?, ?)";
        String itemsTableQuery = "INSERT INTO mealItems(mealID, description) " +
                "VALUES((SELECT MAX(id) FROM meals), ?)";
        PreparedStatement mealPreparedStatement = null;
        PreparedStatement itemPreparedStatement = null;

        try {
            mealPreparedStatement = getConnection().prepareStatement(mealsTableQuery);
            mealPreparedStatement.setInt(1, meal.getDayId());
            mealPreparedStatement.setInt(2, meal.getMealType());

            itemPreparedStatement = getConnection().prepareStatement(itemsTableQuery);
            itemPreparedStatement.setString(1, meal.getDescription());

            getConnection().setAutoCommit(false);
            boolean isSuccessfull = (mealPreparedStatement.executeUpdate() > 0)
                    && (itemPreparedStatement.executeUpdate() > 0);
            getConnection().commit();
            return isSuccessfull;

        } catch (SQLException e) {
            e.printStackTrace();
            try {
                getConnection().rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } finally {
            if (mealPreparedStatement != null) {
                try {
                    mealPreparedStatement.close();
                    if (itemPreparedStatement != null) {
                        itemPreparedStatement.close();
                    }
                    getConnection().setAutoCommit(true);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            closeConnection();

        }
        return false;
    }

    @Override
    public boolean delete(int id) {
        String deleteSQL = "DELETE FROM meals WHERE id = ?";
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = getConnection().prepareStatement(deleteSQL);
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            closeConnection();
        }
        return false;
    }

    @Override
    public boolean update(int id, HashMap<String, String> updateMap) {
        if (updateMap.size() > 0) {
            StringBuilder builder = new StringBuilder("UPDATE mealItems SET ");
            Set<Map.Entry<String, String>> updateSet = updateMap.entrySet();
            Iterator<Map.Entry<String, String>> updateIterator = updateSet.iterator();
            List<String> valueList = new ArrayList<>();
            while (updateIterator.hasNext()) {
                Map.Entry<String, String> updateKeyVal = updateIterator.next();
                String token = updateKeyVal.getKey() + "="
                        + "?"
                        + (updateIterator.hasNext() ? ", " : " ");
                valueList.add(updateKeyVal.getValue());
                builder.append(token);
            }
            builder.append("WHERE mealID=").append(id);
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = getConnection()
                        .prepareStatement(builder.toString());
                for (int c = 1; c <= valueList.size(); c++) {
                    preparedStatement.setString(c, valueList.get(c - 1));
                }

                return preparedStatement.executeUpdate() > 0;

            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
                closeConnection();
            }
        }
        return true;
    }

    @Override
    public List<Meal> getAll() {
        String getMealsQuery = "SELECT mealItems.mealID, description, dayName, mealName FROM mealItems " +
                "INNER JOIN meals ON mealItems.mealID = meals.id " +
                "INNER JOIN days ON meals.dayID = days.id " +
                "INNER JOIN mealTypes ON meals.mealType = mealTypes.id " +
                "ORDER BY days.id, mealTypes.id";

        return templateEngine.query(getMealsQuery, (resultSet, index) -> {
            String description = resultSet.getString("description");
            String dayName = resultSet.getString("dayName");
            String mealName = resultSet.getString("mealName");
            int mealID = resultSet.getInt("mealId");
            return new Meal(mealID, description, dayName, mealName);
        });
    }

    @Override
    public Meal find(int id) {
        return null;
    }

    private Connection getConnection() {
        return createConnection();
    }

    private Connection createConnection() {
        try {
            if (connection == null || connection.isClosed()) {
                try {
                    Class.forName(Constant.DRIVER_NAME);
                    connection = DriverManager
                            .getConnection(Constant.DB_URL, Constant.USER_NAME, Constant.PASSWORD);
                } catch (SQLException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    private void closeConnection() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
