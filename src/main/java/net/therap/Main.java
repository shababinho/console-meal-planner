package net.therap;

import net.therap.util.MyConsole;

import java.sql.SQLException;

/**
 * @author shabab
 * @since 3/11/18
 */
public class Main {

    public static void main(String[] args) throws SQLException {
        new MyConsole().startPrompting();
    }
}
